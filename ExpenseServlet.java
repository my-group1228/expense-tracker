import com.jsonpowerdb.core.*;

public class ExpenseServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String date = request.getParameter("date");
        String category = request.getParameter("category");
        String amount = request.getParameter("amount");
        String description = request.getParameter("description");

        JsonPowerDB jpdb = new JsonPowerDB("<connection-token>", "expense_tracker");
        JSONObject expense = new JSONObject();
        expense.put("date", date);
        expense.put("category", category);
        expense.put("amount", amount);
        expense.put("description", description);
        jpdb.create("expenses", expense.
